using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerData : MonoBehaviour
{

    public string typeClass;
    public float height;
    public float weight;
    public float velocity;
    public float distanceToTravel;
    public int Apple;
    public GameObject TextName;

    

    // Start is called before the first frame update
    private void Start()
    {
        TextName.GetComponent<TMP_Text>().text = PlayerPrefs.GetString("PlayerName");
    }

    // Update is called once per frame
    private void Update()
    {
    }
}