using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    public Animator animation;
    private bool _lookingRight;
    private Vector2 _vectorDireccion = new Vector2(-2f, -1f);
    private Vector2 _vectorDireccion2 = new Vector2(-2f, 3f);
    public GameObject tilemap;
    private bool _canMove = true;

    private void Start()
    {
    }

    private void Update()
    {
        
            if (_canMove)
            {
                if (!_lookingRight)
                {
                    MovementLeft();
                }
                else
                {
                    MovementRight();
                }
        }
        else
        {
            animation.SetBool("IsWalking", false);
        }
            if (_lookingRight)
            {
                _vectorDireccion = new Vector2(2, 0) + Vector2.down;
                _vectorDireccion2 = new Vector2(2, 3);
            }
            else
            {
                _vectorDireccion = new Vector2(-2, 0) + Vector2.down;
                _vectorDireccion2 = new Vector2(-2, 3);
        }
            RaycastHit2D hitFront = Physics2D.Raycast(transform.position, _vectorDireccion, 3.5f, LayerMask.GetMask("Platforms","fireballs"));
            RaycastHit2D hitUp = Physics2D.Raycast(transform.position, _vectorDireccion2, 5.5f, LayerMask.GetMask("fireballs"));

        if (hitFront.collider == null)
            {
                _lookingRight = !_lookingRight;
                _canMove = false;
                Invoke("MovementPermission", 1f);
            }
            else
            {
            if (hitFront.collider.tag == "fireball")
            {

                _lookingRight = !_lookingRight;
                _canMove = false;
                Invoke("MovementPermission", 1f);
            }
            }
        
            if(hitUp.collider != null)
        {
            Debug.Log(hitUp.collider.tag);
            if (hitUp.collider.tag == "fireball")
            {
                _lookingRight = !_lookingRight;
                _canMove = false;
                Invoke("MovementPermission", 1f);
            }
        }
            
        
            
        }
    

    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position, _vectorDireccion);
        Gizmos.DrawRay(transform.position, _vectorDireccion2);
    }

    private void MovementPermission()
    {
        _canMove = true;
    }

    private void MovementRight()
    {
        _lookingRight = true;
        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(2000F * Time.deltaTime, 0));
        gameObject.GetComponent<SpriteRenderer>().flipX = false;
        animation.SetBool("IsWalking", true);
        
    }

    private void MovementLeft()
    {
        _lookingRight = false;
        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-2000F * Time.deltaTime, 0));
        gameObject.GetComponent<SpriteRenderer>().flipX = true;
        animation.SetBool("IsWalking", true);
    }
}