using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public bool canJump;
    public Animator animator;

    // Start is called before the first frame update
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKey("right") || Input.GetKey("d") || Input.GetKey("left") || Input.GetKey("a"))
        {
            Walk();
        }
        else
        {
            animator.SetBool("IsWalking", false);
        }

        if ((Input.GetKeyDown("up") || Input.GetKeyDown("w")) && canJump)
        {
            animator.SetBool("IsJumping", true);
            Jump();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "ground")
        {
            canJump = true;
            animator.SetBool("IsWalking", true);
            animator.SetBool("IsJumping", false);
        }
        

    }
    
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.collider.tag == "ground")
        {
            canJump = true;
            animator.SetBool("IsWalking", true);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "ground")
        {
            canJump = false;
            animator.SetBool("IsWalking", false);
        }
    }

    private void Walk()
    {
        if (Input.GetKey("right") || Input.GetKey("d"))
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(2000F * Time.deltaTime, 0));
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }

        if (Input.GetKey("left") || Input.GetKey("a"))
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-2000F * Time.deltaTime, 0));
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }
    }

    private void Jump()
    {
        if ((Input.GetKeyDown("up") || Input.GetKeyDown("w")) && canJump)
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1000F));
        }
    }
}