using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FireballBehaviour : MonoBehaviour
{
    public Animator animator;
    // Start is called before the first frame update
    private void Start()
    {
        animator.SetBool("Exploding", false);
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            LoadScene();
        }
        if (collision.CompareTag("ground"))
        {
            animator.SetBool("Exploding", true);
            gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY;
            gameObject.GetComponent<CapsuleCollider2D>().enabled = false;
            
            Invoke("DestroyFireball", .55f);
        }
        
    }

    private void LoadScene()
    {
        if (SceneManager.GetActiveScene().name == "Game")
        {
            SceneManager.LoadScene("LoseScreen");

        }
        else
        {
            SceneManager.LoadScene("WinScreen");
        }
    }

    private void DestroyFireball()
    {
        Destroy(gameObject);
    }
}