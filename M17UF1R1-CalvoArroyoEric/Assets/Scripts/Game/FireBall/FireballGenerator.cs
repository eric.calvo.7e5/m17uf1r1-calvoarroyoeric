using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballGenerator : MonoBehaviour
{
    public GameObject fireball;
    public float tiempoSpawn = 0.1f;

    // Start is called before the first frame update
    private void Start()
    {
        FireballSpawner();
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void FireballSpawner()
    {
        Instantiate(fireball, new Vector3(Random.Range(-10, 10), 8f, 0f), Quaternion.identity);
        Invoke("FireballSpawner", tiempoSpawn);
    }
}