using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballSpawner : MonoBehaviour
{
    public GameObject fireball;
    public bool onCooldown;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(onCooldown == false)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector2 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Instantiate(fireball, new Vector2(worldPosition.x, 8f), Quaternion.identity);
                onCooldown = true;
                Invoke("ClickCooldown", 2f);
            }
        }
        
    }

    private void ClickCooldown()
    {
        onCooldown = false;
    }
}
