using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppleBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ground"))
        {
            gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY;
            Invoke("DestroyApple", 8f);
        }
        if (collision.CompareTag("Player"))
        {
            GameObject.Find("Player").GetComponent<Transform>().localScale = new Vector3(2, 2, 2);
            Invoke("ReturnToNormal", 5f);
            gameObject.SetActive(false);
            if (!IsInvoking("ReturnToNormal"))
            {
                Destroy(gameObject);
            }
        }
            
    }
    private void ReturnToNormal()
    {
        GameObject.Find("Player").GetComponent<Transform>().localScale = new Vector3(1, 1, 1);
    }
    private void DestroyApple()
    {
        Destroy(gameObject);
    }
}
