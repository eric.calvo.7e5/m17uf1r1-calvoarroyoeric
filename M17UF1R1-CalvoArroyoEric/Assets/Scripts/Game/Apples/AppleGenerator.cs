using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppleGenerator : MonoBehaviour
{
    public GameObject apple;
    
    private float randomNum;
    // Start is called before the first frame update
    void Start()
    {
        RandomSpawn();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void AppleSpawner()
    {
        Instantiate(apple, new Vector3(Random.Range(-10, 10), 8f, 0f), Quaternion.identity);
    }

    private void RandomSpawn()
    {
        for (int i = 0; i <= 20; i++)
        {
           randomNum = Random.Range(0f, 20f);
        }
        if (randomNum <= 20 && randomNum >= 18)
        {
            AppleSpawner();
            
        }
        Invoke("RandomSpawn", 3f);
    }
}
