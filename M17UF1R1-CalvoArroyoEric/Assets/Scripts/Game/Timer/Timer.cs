using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    public float timerCount = 60f;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<TMP_Text>().text = "1:00";
        TimerDecrease();
    }

    // Update is called once per frame
    void Update()
    {
        if (timerCount == 0)
        {
            LoadScene();
        }
    }
    private void TimerDecrease()
    {
        timerCount--;
        gameObject.GetComponent<TMP_Text>().text = "0:" + timerCount;
        Invoke("TimerDecrease", 1f);
    }
    private void LoadScene()
    {

        if (SceneManager.GetActiveScene().name == "Game1")
        {
            SceneManager.LoadScene("LoseScreen");

        }
        else
        {
            SceneManager.LoadScene("WinScreen");
        }

    }
}
