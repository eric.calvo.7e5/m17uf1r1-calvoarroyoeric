using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class UIController : MonoBehaviour
{

    public GameObject textName;
    public void LoadSceneGame()
    {
        SceneManager.LoadScene("Game");
    }
    public void LoadSceneGame1()
    {
        SceneManager.LoadScene("Game1");
    }
    public void GetName()
    {
        string Nombre = textName.GetComponent<TMP_InputField>().text;
        PlayerPrefs.SetString("PlayerName", Nombre);
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
    }
    void Start()
    {
        
    }
    
    void Update()
    {
        
    }
}
